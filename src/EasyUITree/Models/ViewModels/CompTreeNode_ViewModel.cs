﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyUITree.Models.ViewModels
{
    public class CompTreeNode_ViewModel
    {
        public int NodeId { get; set; }
        public int? ParNodeId { get; set; }
        public int ProgId { get; set; }
        public int Sort { get; set; }
        public string ProgDesc { get; set; }
        public string ProgURL { get; set; }
        /*
        public string ProgArea { get; set; }
        public string ProgController { get; set; }
        public string ProgAction { get; set; }
        */
    }
}
