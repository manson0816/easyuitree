﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EasyUITree.Models.ShcmeaModels
{
    public partial class MenuContext : DbContext
    {
        public virtual DbSet<CompProgram> CompProgram { get; set; }
        public virtual DbSet<CompTreeNode> CompTreeNode { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Menu;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompProgram>(entity =>
            {
                entity.HasKey(e => e.ProgId)
                    .HasName("PK_CompProgram");

                entity.HasIndex(e => e.ProgNo)
                    .HasName("IK_CompProgram_ProgNo")
                    .IsUnique();

                entity.Property(e => e.ProgId).HasColumnName("ProgID");

                entity.Property(e => e.CreateDatetime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.CreateEmpId)
                    .IsRequired()
                    .HasColumnName("CreateEmpID")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ModifyDatetime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.ModifyEmpId)
                    .IsRequired()
                    .HasColumnName("ModifyEmpID")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ProgCategory)
                    .IsRequired()
                    .HasColumnType("varchar(1)");

                entity.Property(e => e.ProgDesc)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ProgNo)
                    .IsRequired()
                    .HasColumnType("varchar(10)");
            });

            modelBuilder.Entity<CompTreeNode>(entity =>
            {
                entity.HasKey(e => e.NodeId)
                    .HasName("PK_CompTreeNode");

                entity.Property(e => e.NodeId)
                    .HasColumnName("NodeID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreateDatetime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.CreateEmpId)
                    .IsRequired()
                    .HasColumnName("CreateEmpID")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ModifyDatetime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.ModifyEmpId)
                    .IsRequired()
                    .HasColumnName("ModifyEmpID")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ParNodeId).HasColumnName("ParNodeID");

                entity.Property(e => e.ProgId).HasColumnName("ProgID");

                entity.HasOne(d => d.Node)
                    .WithOne(p => p.CompTreeNode)
                    .HasForeignKey<CompTreeNode>(d => d.NodeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CompTreeNode_CompProgram");

                entity.HasOne(d => d.ParNode)
                    .WithMany(p => p.InverseParNode)
                    .HasForeignKey(d => d.ParNodeId)
                    .HasConstraintName("FK_CompTreeNode_CompTreeNode");
            });
        }
    }
}