﻿using System;
using System.Collections.Generic;

namespace EasyUITree.Models.ShcmeaModels
{
    public partial class CompProgram
    {
        public int ProgId { get; set; }
        public string ProgNo { get; set; }
        public string ProgDesc { get; set; }
        public string ProgCategory { get; set; }
        public string ProgArea { get; set;}
        public string ProgController { get; set; }
        public string ProgAction { get; set; }
        public DateTime CreateDatetime { get; set; }
        public string CreateEmpId { get; set; }
        public DateTime ModifyDatetime { get; set; }
        public string ModifyEmpId { get; set; }

        public virtual CompTreeNode CompTreeNode { get; set; }
    }
}
