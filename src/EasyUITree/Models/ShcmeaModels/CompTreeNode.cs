﻿using System;
using System.Collections.Generic;

namespace EasyUITree.Models.ShcmeaModels
{
    public partial class CompTreeNode
    {
        public int NodeId { get; set; }
        public int? ParNodeId { get; set; }
        public int ProgId { get; set; }
        public int Sort { get; set; }
        public int? Status { get; set; }
        public DateTime CreateDatetime { get; set; }
        public string CreateEmpId { get; set; }
        public DateTime ModifyDatetime { get; set; }
        public string ModifyEmpId { get; set; }

        public virtual CompProgram Node { get; set; }
        public virtual CompTreeNode ParNode { get; set; }
        public virtual ICollection<CompTreeNode> InverseParNode { get; set; }
    }
}
