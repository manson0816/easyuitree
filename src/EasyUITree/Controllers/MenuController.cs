﻿using Microsoft.AspNetCore.Mvc;
using EasyUITree.Repositorys;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EasyUITree.Controllers
{
    public class MenuController : Controller
    {
        private MenuTree_Repository _helper = new MenuTree_Repository();

        // GET: /<controller>/
        public IActionResult TreeLoadJson()
        {
            ViewBag.HasRootNode = this._helper.GetRootNode() != null ? "true" : "false";
            return View();
        }

        [HttpPost]
        public ActionResult GetTreeNodeJSON()
        {
            string result = this._helper.GetTreeJson();
            return Content(result, "application/json");
            //ViewBag.jsonData = this._helper.GetTreeJson();
            //return View();
        }
    }
}
