﻿using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EasyUITree.Models.ShcmeaModels;
using EasyUITree.Models.ViewModels;

namespace EasyUITree.Repositorys
{
    public class MenuTree_Repository
    {
        private MenuContext db;
        private List<CompTreeNode_ViewModel> EntityTreeNode;

        public MenuTree_Repository()
        {
            db = new MenuContext();

            EntityTreeNode = (
                from a in db.CompTreeNode
                join b in db.CompProgram
                on a.ProgId equals b.ProgId
                select new CompTreeNode_ViewModel
                {
                    NodeId = a.NodeId,
                    ParNodeId = a.ParNodeId,
                    ProgId = a.ProgId,
                    ProgDesc = b.ProgDesc,
                    Sort = a.Sort,
                    ProgURL = b.ProgCategory == "F" ? "/" + b.ProgArea + "/" + b.ProgController + "/" + b.ProgAction : "#"

                    //      "/" + b.ProgArea + "/" + b.ProgController + "/" + b.ProgAction
                    /*
                    ProgArea = b.ProgArea,
                    ProgController = b.ProgController,
                    ProgAction = b.ProgAction
                    */
                }
            ).ToList();

        }

        public CompTreeNode_ViewModel GetRootNode()
        {
            return EntityTreeNode.FirstOrDefault(x => !x.ParNodeId.HasValue);
        }

        public IQueryable<CompTreeNode_ViewModel> GetNodes(bool isReadAll = false)
        {
            var query = EntityTreeNode.AsQueryable();
            if (!isReadAll)
            {
                return query;
            }
            return query;
        }

        public string GetTreeJson()
        {

            List<JObject> jObjects = new List<JObject>();

            var allNodes = EntityTreeNode.ToList();
            if (allNodes.Any())
            {
                var rootNode = this.GetRootNode();
                JObject root = new JObject
                {
                    {"id", rootNode.NodeId.ToString()},
                    {"text", rootNode.ProgDesc}
                };
                root.Add("children", this.GetChildArray(rootNode, allNodes));
                jObjects.Add(root);
            }

            return JsonConvert.SerializeObject(jObjects);

        }

        private JArray GetChildArray(CompTreeNode_ViewModel parentNode, IEnumerable<CompTreeNode_ViewModel> nodes)
        {
            JArray childArray = new JArray();

            foreach (var node in nodes.Where(x => x.ParNodeId == parentNode.NodeId))
            {
                JObject subObject = new JObject
                {
                    {"id", node.NodeId.ToString()},
                    {"text", node.ProgDesc},
                    {"attributes", new JObject {
                        {"url", node.ProgURL }
                    }}
                    /*
                    {"id", node.NodeId.ToString()},
                    {"text", node.ProgDesc}
                    */
                };

                //{ "attributes", "/" + node.ProgArea + "/" + node.ProgController + "/" + node.ProgAction }

                if (nodes.Where(y => y.ParNodeId == node.NodeId).Any())
                {
                    subObject.Add("children", this.GetChildArray(node, nodes));
                }
                childArray.Add(subObject);
            }

            return childArray;
        }


    }
}
