-- 功能資料檔
CREATE TABLE CompProgram (
    ProgID         INT IDENTITY (1, 1) NOT NULL,
    ProgNo         VARCHAR (10) NOT NULL,
    ProgDesc       VARCHAR (20) NOT NULL,
    ProgCategory   VARCHAR (1)  NOT NULL,
    ProgArea	     VARCHAR (30),
    ProgController VARCHAR (30),
    ProgAction     VARCHAR (30),
    CreateDatetime DATETIME     DEFAULT (getdate()) NOT NULL,
    CreateEmpID    VARCHAR (10) NOT NULL,
    ModifyDatetime DATETIME     DEFAULT (getdate()) NOT NULL,
    ModifyEmpID    VARCHAR (10) NOT NULL,
    CONSTRAINT PK_CompProgram PRIMARY KEY CLUSTERED (ProgID ASC),
    CONSTRAINT IK_CompProgram_ProgNo UNIQUE NONCLUSTERED (ProgNo ASC),
    CONSTRAINT CK_CompProgram_ProgNo CHECK (ProgNo=upper(ProgNo))
);

SET IDENTITY_INSERT [dbo].[CompProgram] ON
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (3, N'COMPE0010', N'公司代碼維護', N'F', N'Menu', N'Comp', N'CompList', N'2017-03-14 16:16:49', N'1', N'2017-03-14 16:16:49', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (4, N'COMPE0020', N'使用者資料維護', N'F', N'Menu', N'Comp', N'UserList', N'2017-03-14 16:17:04', N'1', N'2017-03-14 16:17:04', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (5, N'COMPE0030', N'程式名稱登錄', N'F', N'Menu', N'Comp', N'ProgList', N'2017-03-14 16:17:33', N'1', N'2017-03-14 16:17:33', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (29, N'COMPE0040', N'程式樹狀結構維護', N'F', N'Menu', N'Comp', N'MenuList', N'2017-03-22 13:23:01', N'1', N'2017-03-22 13:23:01', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (2, N'COMPMENU', N'系統權限管理', N'M', N'', NULL, NULL, N'2017-03-14 16:14:33', N'1', N'2017-03-14 16:14:33', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (27, N'FMGLMENU', N'總帳系統', N'M', NULL, NULL, NULL, N'2017-03-16 10:25:12', N'1', N'2017-03-16 10:25:12', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (26, N'FMIVB0010', N'申報發票轉文字檔', N'F', N'FMIV', NULL, NULL, N'2017-03-14 16:24:32', N'1', N'2017-03-14 16:24:32', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (22, N'FMIVE0010', N'發票購入作業', N'F', N'FMIV', NULL, NULL, N'2017-03-14 16:22:45', N'1', N'2017-03-14 16:22:45', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (23, N'FMIVE0020', N'發票分配作業', N'F', N'FMIV', NULL, NULL, N'2017-03-14 16:23:04', N'1', N'2017-03-14 16:23:04', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (24, N'FMIVE0210', N'進項發票轉入作業', N'F', N'FMIV', NULL, NULL, N'2017-03-14 16:23:38', N'1', N'2017-03-14 16:23:38', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (25, N'FMIVE0220', N'銷項發票轉入作業', N'F', N'FMIV', NULL, NULL, N'2017-03-14 16:23:56', N'1', N'2017-03-14 16:23:56', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (17, N'FMIVMENU', N'發票管理系統', N'M', N'', NULL, NULL, N'2017-03-14 16:21:03', N'1', N'2017-03-14 16:21:03', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (18, N'FMIVMENU01', N'基本資料維護作業', N'M', NULL, NULL, NULL, N'2017-03-14 16:21:30', N'1', N'2017-03-14 16:21:30', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (19, N'FMIVMENU02', N'發票轉入作業', N'M', NULL, NULL, NULL, N'2017-03-14 16:22:01', N'1', N'2017-03-14 16:22:01', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (21, N'FMIVMENU03', N'發票申報作業', N'M', NULL, NULL, NULL, N'2017-03-14 16:22:23', N'1', N'2017-03-14 16:22:23', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (1, N'TOPMENU', N'系統功能表', N'M', NULL, NULL, NULL, N'2017-03-14 16:14:24', N'1', N'2017-03-14 16:14:24', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (9, N'UTLEE0010', N'幣別維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:19:12', N'1', N'2017-03-14 16:19:12', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (11, N'UTLEE0020', N'銀行維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:19:38', N'1', N'2017-03-14 16:19:38', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (12, N'UTLEE0030', N'匯率維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:19:46', N'1', N'2017-03-14 16:19:46', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (13, N'UTLEE0040', N'部門維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:19:54', N'1', N'2017-03-14 16:19:54', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (14, N'UTLEE0050', N'客戶基本檔維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:20:01', N'1', N'2017-03-14 16:20:01', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (16, N'UTLEE0060', N'廠商基本檔維護', N'F', NULL, NULL, NULL, N'2017-03-14 16:20:39', N'1', N'2017-03-14 16:20:39', N'1')
INSERT INTO [dbo].[CompProgram] ([ProgID], [ProgNo], [ProgDesc], [ProgCategory], [ProgArea], [ProgController], [ProgAction], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (7, N'UTLTMENU', N'基本資料管理', N'M', NULL, NULL, NULL, N'2017-03-14 16:18:44', N'1', N'2017-03-14 16:18:44', N'1')
SET IDENTITY_INSERT [dbo].[CompProgram] OFF
