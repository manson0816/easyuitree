-- 程式節點
CREATE TABLE CompTreeNode (
    NodeID                  INT          IDENTITY (1, 1) NOT NULL,
    ParNodeID               INT          NULL,
    ProgID                  INT          NOT NULL,
    Sort                    INT          NOT NULL,
    Status                  INT          NULL,
    CreateDatetime          DATETIME     DEFAULT (getdate()) NOT NULL,
    CreateEmpID             VARCHAR (10) NOT NULL,
    ModifyDatetime          DATETIME     DEFAULT (getdate()) NOT NULL,
    ModifyEmpID             VARCHAR (10) NOT NULL,
    CONSTRAINT PK_CompTreeNode PRIMARY KEY CLUSTERED (NodeID ASC),
	  CONSTRAINT FK_CompTreeNode_CompTreeNode FOREIGN KEY (ParNodeID) REFERENCES CompTreeNode (NodeID),
    CONSTRAINT FK_CompTreeNode_CompProgram FOREIGN KEY (NodeID) REFERENCES CompProgram (ProgID)
);

SET IDENTITY_INSERT [dbo].[CompTreeNode] ON
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (5, NULL, 1, 10, 1, N'2017-03-15 16:17:30', N'1', N'2017-03-15 16:17:30', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (7, 5, 2, 20, 1, N'2017-03-15 16:17:56', N'1', N'2017-03-15 16:17:56', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (11, 7, 3, 200010, 1, N'2017-03-15 16:18:53', N'1', N'2017-03-15 16:18:53', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (12, 7, 4, 200020, 1, N'2017-03-15 16:19:15', N'1', N'2017-03-15 16:19:15', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (13, 7, 5, 200030, 1, N'2017-03-15 16:19:32', N'1', N'2017-03-15 16:19:32', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (14, 5, 7, 30, 1, N'2017-03-15 16:19:59', N'1', N'2017-03-15 16:19:59', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (16, 14, 9, 300010, 1, N'2017-03-15 16:20:22', N'1', N'2017-03-15 16:20:22', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (17, 14, 11, 300020, 1, N'2017-03-15 16:20:29', N'1', N'2017-03-15 16:20:29', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (18, 14, 12, 300030, 1, N'2017-03-15 16:20:37', N'1', N'2017-03-15 16:20:37', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (19, 14, 13, 300040, 1, N'2017-03-15 16:20:44', N'1', N'2017-03-15 16:20:44', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (21, 14, 14, 300050, 1, N'2017-03-15 16:21:10', N'1', N'2017-03-15 16:21:10', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (22, 14, 16, 300060, 1, N'2017-03-15 16:21:27', N'1', N'2017-03-15 16:21:27', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (23, 5, 17, 40, 1, N'2017-03-15 16:22:01', N'1', N'2017-03-15 16:22:01', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (24, 23, 18, 4010, 1, N'2017-03-15 16:37:27', N'1', N'2017-03-15 16:37:27', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (25, 23, 19, 4020, 1, N'2017-03-15 16:37:49', N'1', N'2017-03-15 16:37:49', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (26, 23, 21, 4030, 1, N'2017-03-15 16:38:00', N'1', N'2017-03-15 16:38:00', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (48, 24, 22, 40100010, 1, N'2017-03-15 16:49:42', N'1', N'2017-03-15 16:49:42', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (49, 24, 23, 40100020, 1, N'2017-03-15 16:49:49', N'1', N'2017-03-15 16:49:49', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (50, 25, 24, 40200010, 1, N'2017-03-15 16:50:32', N'1', N'2017-03-15 16:50:32', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (51, 25, 25, 40200020, 1, N'2017-03-15 16:50:41', N'1', N'2017-03-15 16:50:41', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (52, 26, 26, 40300010, 1, N'2017-03-15 16:50:54', N'1', N'2017-03-15 16:50:54', N'1')
INSERT INTO [dbo].[CompTreeNode] ([NodeID], [ParNodeID], [ProgID], [Sort], [Status], [CreateDatetime], [CreateEmpID], [ModifyDatetime], [ModifyEmpID]) VALUES (53, 5, 27, 50, 1, N'2017-03-16 10:25:44', N'1', N'2017-03-16 10:25:44', N'1')
SET IDENTITY_INSERT [dbo].[CompTreeNode] OFF
